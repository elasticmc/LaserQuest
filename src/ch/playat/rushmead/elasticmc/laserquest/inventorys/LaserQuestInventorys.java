package ch.playat.rushmead.elasticmc.laserquest.inventorys;

import ch.playat.rushmead.elasticmc.laserquest.inventorys.gunbuilder.GunBuilderBarrels;
import ch.playat.rushmead.elasticmc.laserquest.inventorys.gunbuilder.GunBuilderCases;
import ch.playat.rushmead.elasticmc.laserquest.inventorys.gunbuilder.GunBuilderCoolers;
import ch.playat.rushmead.elasticmc.laserquest.inventorys.gunbuilder.GunBuilderLasers;
import ch.playat.rushmead.elasticmc.laserquest.inventorys.gunbuilder.GunBuilderMain;
import ch.playat.rushmead.elasticmc.laserquest.inventorys.gunbuilder.GunBuilderStore;

/**
 *
 * @author Rushmead
 */
public class LaserQuestInventorys {

    private static TeamSelector teamSelector = new TeamSelector();
    private static GunBuilderMain gunBuilderMain = new GunBuilderMain();
    private static GunBuilderStore gunBuilderStore = new GunBuilderStore();
    private static GunBuilderBarrels gunBuilderBarrels = new GunBuilderBarrels();
    private static GunBuilderLasers gunBuilderLasers = new GunBuilderLasers();
    private static GunBuilderCoolers gunBuilderCoolers = new GunBuilderCoolers();
    private static GunBuilderCases gunBuilderCases = new GunBuilderCases();

    public static TeamSelector getTeamSelector() {
        return teamSelector;
    }

    public static GunBuilderMain getGunBuilderMain() {
        return gunBuilderMain;
    }

    public static GunBuilderStore getGunBuilderStore() {
        return gunBuilderStore;
    }

    public static GunBuilderBarrels getGunBuilderBarrels() {
        return gunBuilderBarrels;
    }

    /**
     * @return the gunBuilderLasers
     */
    public static GunBuilderLasers getGunBuilderLasers() {
        return gunBuilderLasers;
    }

    /**
     * @return the gunBuilderCoolers
     */
    public static GunBuilderCoolers getGunBuilderCoolers() {
        return gunBuilderCoolers;
    }

    /**
     * @return the gunBuilderCases
     */
    public static GunBuilderCases getGunBuilderCases() {
        return gunBuilderCases;
    }
}

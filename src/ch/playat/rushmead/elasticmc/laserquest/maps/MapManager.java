package ch.playat.rushmead.elasticmc.laserquest.maps;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 *
 * @author Rushmead
 */
public class MapManager {

    private HashMap<String, Map> maps = new HashMap<String, Map>();
    private Map currentMap;

    public synchronized void loadMaps() {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `maps` WHERE `game`=?");
            ps.setString(1, "laserquest");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Map m = new Map(rs.getString("name"), rs.getString("authors"), getDeserializedLocation(rs.getString("spawnLocation1")), getDeserializedLocation(rs.getString("spawnLocation2")));
                maps.put(rs.getString("name"), m);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void pickMap() {
        Random generator = new Random();
        Object[] values = maps.values().toArray();
        Map randomValue = (Map) values[generator.nextInt(values.length)];
        currentMap = randomValue;
    }

    public Map getCurrentMap() {
        return currentMap;
    }

    public Map getMapByName(String name) {
        return maps.get(name);
    }

    public HashMap<String, Map> getMaps() {
        return maps;
    }

    public String getSerializedLocation(Location loc) { //Converts location -> String
        return loc.getX() + ";" + loc.getY() + ";" + loc.getZ();
        //feel free to use something to split them other than semicolons (Don't use periods or numbers)
    }

    public Location getDeserializedLocation(String s) {//Converts String -> Location
        String[] parts = s.split(";"); //If you changed the semicolon you must change it here too
        double x = Double.parseDouble(parts[0]);
        double y = Double.parseDouble(parts[1]);
        double z = Double.parseDouble(parts[2]);
        World w = Bukkit.getServer().getWorlds().get(0);
        return new Location(w, x, y, z); //can return null if the world no longer exists
    }
}

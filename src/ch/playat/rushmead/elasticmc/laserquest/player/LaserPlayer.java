package ch.playat.rushmead.elasticmc.laserquest.player;

import ch.playat.rushmead.elasticmc.laserquest.game.GameRank;
import ch.playat.rushmead.elasticmc.laserquest.game.guns.*;
import ch.playat.rushmead.elasticmc.laserquest.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rushmead
 */
public class LaserPlayer {

    private static LaserPlayerDB hpDB = new LaserPlayerDB();
    private static HashMap<String, LaserPlayer> players = new HashMap<>();

    public static HashMap<String, LaserPlayer> getPlayers() {
        return players;
    }

    public static LaserPlayer getPlayer(String name) {
        if (!players.containsKey(name)) {
            return null;
        }
        return players.get(name);
    }

    public static LaserPlayerDB getLaserDB() {
        return hpDB;
    }

    public static LaserPlayer createPlayer(Player p) {
        if (!players.containsKey(p.getDisplayName())) {
            LaserPlayer player = hpDB.getPlayer(p);
            if (player == null && p != null) {
                player = new LaserPlayer(p.getDisplayName());
            }
            players.put(p.getDisplayName(), player);
            return player;
        } else {
            return getPlayer(p.getDisplayName());
        }

    }

    public static void removePlayer(String name) {
        if (players.containsKey(name)) {
            players.remove(name);

        }
    }

    private String realName;
    // Per Instance Stuff
    private int points;
    private Team team;
    private boolean deactivated;
    private int deactivateTime;
    private int coolDown;
//Database Stuff
    private Gun gun;
    private int totalPoints;
    private List<Barrel> unlockedBarrels;
    private List<Laser> unlockedLasers;
    private List<Case> unlockedCases;
    private List<Cooler> unlockedCoolers;
    private GameRank gameRank;
    private int shots;
    private int deaths;
    public LaserPlayer(String name) {
        this.realName = name;
        this.points = 0;
        this.team = null;
        this.deactivated = false;
        this.deactivateTime = 0;
        this.gun = new Gun("Basic Shooter", Barrel.DEFAULT, Case.DEFAULT, Cooler.DEFAULT, Laser.DEFAULT);
        this.totalPoints = 0;
        this.coolDown = 0;
        this.unlockedBarrels = new ArrayList<>();
        unlockedBarrels.add(Barrel.DEFAULT);
        this.unlockedLasers = new ArrayList<>();
        unlockedLasers.add(Laser.DEFAULT);
        this.unlockedCases = new ArrayList<>();
        unlockedCases.add(Case.DEFAULT);
        this.unlockedCoolers = new ArrayList<>();
        unlockedCoolers.add(Cooler.DEFAULT);
        this.setGameRank(GameRank.PRIVATE);
        this.setShots(0);
        this.setDeaths(0);
    }

    public LaserPlayer(String name, String uuid, Gun g, int totalPoints, String barrels, String lasers, String cases, String coolers, GameRank gameRank, int shots, int deaths) {
        this.realName = name;
        this.points = 0;
        this.team = null;
        this.deactivated = false;
        this.deactivateTime = 0;
        this.gun = g;
        this.coolDown = 0;
        this.totalPoints = totalPoints;
        this.unlockedBarrels = getUnlockedBarrels(barrels);
        this.unlockedCases = getUnlockedCases(cases);
        this.unlockedCoolers = getUnlockedCoolers(coolers);
        this.unlockedLasers = getUnlockedLasers(lasers);
        this.setDeaths(deaths);
        this.setGameRank(gameRank);
        this.setShots(shots);
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(getRealName());
    }

    /**
     * @return the realName
     */
    public String getRealName() {
        return realName;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @return the team
     */
    public Team getTeam() {
        return team;
    }

    /**
     * @param team the team to set
     */
    public void setTeam(Team team) {
        this.team = team;
    }

    /**
     * @return the deactivated
     */
    public boolean isDeactivated() {
        return deactivated;
    }

    /**
     * @param deactivated the deactivated to set
     */
    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }

    /**
     * @return the gun
     */
    public Gun getGun() {
        return gun;
    }

    /**
     * @param gun the gun to set
     */
    public void setGun(Gun gun) {
        this.gun = gun;
    }

    /**
     * @return the totalPoints
     */
    public int getTotalPoints() {
        return totalPoints;
    }

    /**
     * @param totalPoints the totalPoints to set
     */
    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    /**
     * @return the deactivateTime
     */
    public int getDeactivateTime() {
        return deactivateTime;
    }

    /**
     * @param deactivateTime the deactivateTime to set
     */
    public void setDeactivateTime(int deactivateTime) {
        this.deactivateTime = deactivateTime;
    }

    /**
     * @return the coolDown
     */
    public int getCoolDown() {
        return coolDown;
    }

    /**
     * @param coolDown the coolDown to set
     */
    public void setCoolDown(int coolDown) {
        this.coolDown = coolDown;
    }

    public static List<Barrel> getUnlockedBarrels(String toys) {
        String[] toyStrs = toys.split(",");
        List<Barrel> unlockedToys = new ArrayList<>();
        HashMap<Integer, Barrel> allBarrels = new HashMap<>();

        for (Barrel toy : Barrel.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (String toyStr : toyStrs) {
            unlockedToys.add(allBarrels.get(Integer.parseInt(toyStr)));
        }

        return unlockedToys;
    }

    public static List<Laser> getUnlockedLasers(String toys) {
        String[] toyStrs = toys.split(",");
        List<Laser> unlockedToys = new ArrayList<>();
        HashMap<Integer, Laser> allBarrels = new HashMap<>();

        for (Laser toy : Laser.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (String toyStr : toyStrs) {
            unlockedToys.add(allBarrels.get(Integer.parseInt(toyStr)));
        }

        return unlockedToys;
    }

    public static List<Case> getUnlockedCases(String toys) {
        String[] toyStrs = toys.split(",");
        List<Case> unlockedToys = new ArrayList<>();
        HashMap<Integer, Case> allBarrels = new HashMap<>();

        for (Case toy : Case.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (String toyStr : toyStrs) {
            unlockedToys.add(allBarrels.get(Integer.parseInt(toyStr)));
        }

        return unlockedToys;
    }

    public static List<Cooler> getUnlockedCoolers(String toys) {
        String[] toyStrs = toys.split(",");
        List<Cooler> unlockedToys = new ArrayList<>();
        HashMap<Integer, Cooler> allBarrels = new HashMap<>();

        for (Cooler toy : Cooler.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (String toyStr : toyStrs) {
            unlockedToys.add(allBarrels.get(Integer.parseInt(toyStr)));
        }

        return unlockedToys;
    }

    public void unlockBarrel(Barrel b) {
        if (!unlockedBarrels.contains(b)) {
            getUnlockedBarrels().add(b);
        }
    }

    public void unlockLaser(Laser b) {
        if (!unlockedLasers.contains(b)) {
            getUnlockedLasers().add(b);
        }
    }

    public void unlockCooler(Cooler b) {
        if (!unlockedCoolers.contains(b)) {
            getUnlockedCoolers().add(b);
        }
    }

    public void unlockCase(Case b) {
        if (!unlockedCases.contains(b)) {
            getUnlockedCases().add(b);
        }
    }

    /**
     * @return the unlockedBarrels
     */
    public List<Barrel> getUnlockedBarrels() {
        return unlockedBarrels;
    }

    /**
     * @return the unlockedLasers
     */
    public List<Laser> getUnlockedLasers() {
        return unlockedLasers;
    }

    /**
     * @return the unlockedCases
     */
    public List<Case> getUnlockedCases() {
        return unlockedCases;
    }

    /**
     * @return the unlockedCoolers
     */
    public List<Cooler> getUnlockedCoolers() {
        return unlockedCoolers;
    }

    public boolean hasBarrel(Barrel b) {
        return unlockedBarrels.contains(b);
    }

    public boolean hasLaser(Laser b) {
        return unlockedLasers.contains(b);
    }

    public boolean hasCases(Case b) {
        return unlockedCases.contains(b);
    }

    public boolean hasCooler(Cooler b) {
        return unlockedCoolers.contains(b);
    }

    public String getUnlockedBarrelsString() {
        String unlocked = "1,";

        for (Barrel toy : getUnlockedBarrels()) {
            if (toy.getId() != 0) {
                unlocked += toy.getId() + ",";
            }
        }

        return unlocked;
    }

    public String getUnlockedLasersString() {
        String unlocked = "1,";

        for (Laser toy : getUnlockedLasers()) {
            if (toy.getId() != 0) {
                unlocked += toy.getId() + ",";
            }
        }

        return unlocked;
    }

    public String getUnlockedCasesString() {
        String unlocked = "1,";

        for (Case toy : getUnlockedCases()) {
            if (toy.getId() != 0) {
                unlocked += toy.getId() + ",";
            }
        }

        return unlocked;
    }

    public String getUnlockedCoolersString() {
        String unlocked = "1,";

        for (Cooler toy : getUnlockedCoolers()) {
            if (toy.getId() != 0) {
                unlocked += toy.getId() + ",";
            }
        }

        return unlocked;
    }

    public GameRank getGameRank() {
        return gameRank;
    }

    public void setGameRank(GameRank gameRank) {
        this.gameRank = gameRank;
    }

    public int getShots() {
        return shots;
    }

    public void setShots(int shots) {
        this.shots = shots;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }
}

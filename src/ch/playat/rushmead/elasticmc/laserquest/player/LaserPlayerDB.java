package ch.playat.rushmead.elasticmc.laserquest.player;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.laserquest.game.GameRank;
import ch.playat.rushmead.elasticmc.laserquest.game.guns.*;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class LaserPlayerDB {

    /**
     * Add a player (with default hub player data) to the database
     *
     * @param uuid The UUID of the player (in string form)
     */
    public synchronized void addPlayerToDatabase(String uuid) {
        if (isInDatabase(uuid)) {
            return;
        }
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `playerdata_laserquest`(`uuid`) VALUES (?)");
            ps.setString(1, uuid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Check if a UUID is already in the database
     *
     * @param uuid The player UUID (in string form)
     *
     * @return Is the player in the database?
     */
    public synchronized boolean isInDatabase(String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `uuid` FROM `playerdata_laserquest` WHERE `uuid` = ?");
            ps.setString(1, uuid);
            
            ResultSet rs = ps.executeQuery();
            boolean has = rs.next();
            
            if (has) {
                return true;
            }
        } catch (SQLException ex) {
            
        }
        
        return false;
    }

    /**
     * Return an Hub Player from a Player's name
     *
     * @param p The player
     *
     * @return The Hub player
     */
    public synchronized LaserPlayer getPlayer(Player p) {
        LaserPlayer player = null;
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `playerdata_laserquest` WHERE `uuid` = ?");
            ps.setString(1, p.getUniqueId().toString());
            
            ResultSet rs = ps.executeQuery();
            boolean has = rs.next();
            
            if (has) {
                Gun g = null;
                if (rs.getString("gun").isEmpty()) {
                    
                    g = new Gun("Basic Shooter", Barrel.DEFAULT, Case.DEFAULT, Cooler.DEFAULT, Laser.DEFAULT);
                } else {
                    
                    g = Gun.getGunByString(rs.getString("gun"));
                }
                player = new LaserPlayer(p.getDisplayName(), p.getUniqueId().toString(), g, rs.getInt("total"), rs.getString("barrels"), rs.getString("lasers"), rs.getString("cases"), rs.getString("coolers"), GameRank.getRankFromID(rs.getInt("rank")), rs.getInt("shots"), rs.getInt("deaths"));
                return player;
            } else {
                throw new SQLException();
            }
        } catch (SQLException ex) {
            addPlayerToDatabase(p.getUniqueId().toString());
            player = new LaserPlayer(p.getDisplayName());
        }
        
        return player;
    }

    /**
     * Save a player's information to the database
     *
     * @param player The player you are saving to the database
     */
    public synchronized void savePlayerToDB(LaserPlayer player) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `playerdata_laserquest` SET `gun` = ?, `total` = ?, `barrels` =?, `lasers` =?, `cases`=? , `coolers`=?, `rank`=?, `shots`=?, `deaths`=? WHERE `uuid` = ?");
            ps.setString(1, player.getGun().asString());
            ps.setInt(2, player.getTotalPoints());
            ps.setString(3, player.getUnlockedBarrelsString());
            ps.setString(4, player.getUnlockedLasersString());
            ps.setString(5, player.getUnlockedCasesString());
            ps.setString(6, player.getUnlockedCoolersString());
            ps.setInt(7, player.getGameRank().getId());
            ps.setInt(8, player.getShots());
            ps.setInt(9, player.getDeaths());
            ps.setString(10, player.getPlayer().getUniqueId().toString());
            ps.executeUpdate();
            ps.close();
        } catch (NullPointerException ex) {
            
        } catch (SQLException ex) {
            this.addPlayerToDatabase(player.getPlayer().getUniqueId().toString());
        }
    }
}

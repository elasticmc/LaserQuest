package ch.playat.rushmead.elasticmc.laserquest.events;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.customevents.OneMinuteTimerEvent;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.laserquest.LaserQuest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Rushmead
 */
public class OneMinute implements Listener {

    @EventHandler
    public void onMinute(OneMinuteTimerEvent e) {
        if (GameState.getGameState() == GameState.IN_GAME) {
            if (LaserQuest.getInstance().getGameManager().gameTime == 1 && !LaserQuest.getInstance().getGameManager().countdown) {
                LaserQuest.getInstance().getGameManager().countDownTime = 60;
                LaserQuest.getInstance().getGameManager().countdown = true;
            } else if (LaserQuest.getInstance().getGameManager().gameTime == 0) {
                LaserQuest.getInstance().getGameManager().stop();
                LaserQuest.getInstance().getGameManager().countdown = false;
            } else {
                Messaging.broadcastMessage("&6&l{0} &5Minutes Remaining", LaserQuest.getInstance().getGameManager().gameTime);
                LaserQuest.getInstance().getGameManager().gameTime -= 1;
            }
        }
    }
}

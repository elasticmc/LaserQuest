

package ch.playat.rushmead.elasticmc.laserquest.events;


import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.framework.Framework;
import ch.playat.rushmead.elasticmc.framework.api.ServerSending;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent.Status;
/**
 *
 * @author Rushmead
 */
public class ResourcePack implements Listener{

        @EventHandler
        public void onEvent(PlayerResourcePackStatusEvent e){
            if(e.getStatus().equals(Status.DECLINED) || e.getStatus().equals(Status.FAILED_DOWNLOAD)){
                Messaging.sendMessage(e.getPlayer(), "&cYou were kicked from {0} because you didnt have the resourcepack", Servers.getData().getName());
                ServerSending.sendToServer(e.getPlayer(), "hub1");

            }
        }
}

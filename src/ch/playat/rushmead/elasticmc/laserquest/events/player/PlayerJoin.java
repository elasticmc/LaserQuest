package ch.playat.rushmead.elasticmc.laserquest.events.player;

import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.laserquest.LaserQuest;
import ch.playat.rushmead.elasticmc.laserquest.lobby.LobbyItems;
import ch.playat.rushmead.elasticmc.laserquest.player.LaserPlayer;
import ch.playat.rushmead.elasticmc.laserquest.util.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        LaserPlayer lp = LaserPlayer.createPlayer(e.getPlayer());
        Messaging.broadcastMessage("{0} has Joined, {1}/{2}.", e.getPlayer().getName(), Bukkit.getOnlinePlayers().size(), 12);
        if (Bukkit.getOnlinePlayers().size() >= 8) {
            Messaging.broadcastMessage("Minimum players reached. Starting Countdown...");
            LaserQuest.getInstance().getLobbyManager().startCountdown();
        }

        e.getPlayer().teleport(new Location(Bukkit.getWorlds().get(0), -299, 80, -298));
        e.getPlayer().getInventory().clear();
        e.getPlayer().getActivePotionEffects().clear();
        for (int i = 0; i < LobbyItems.getInventory().length; i++) {
            if (LobbyItems.getInventory()[i] == null) {
                continue;
            }
            e.getPlayer().getInventory().setItem(i, LobbyItems.getInventory()[i]);
        }
        Scoreboard.lobby(e.getPlayer());
        e.getPlayer().setResourcePack("https://www.dropbox.com/s/89szgw732alpt13/LaserQuestRp1.1.1.zip?dl=1");
        for (Player ps : Bukkit.getOnlinePlayers()) {
            Scoreboard.lobby(ps);
        }
        ElasticPlayer ep = ElasticPlayer.getPlayer(e.getPlayer().getName());

        ep.setChatPrefix(ChatColor.GRAY + lp.getGameRank().getDisplayName());
    }
}

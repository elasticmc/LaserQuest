package ch.playat.rushmead.elasticmc.laserquest.events.player;

import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.laserquest.game.guns.Gun;
import ch.playat.rushmead.elasticmc.laserquest.inventorys.LaserQuestInventorys;
import ch.playat.rushmead.elasticmc.laserquest.player.LaserPlayer;
import ch.playat.rushmead.elasticmc.laserquest.util.ParticleEffect;
import org.bukkit.Color;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

/**
 * @author Rushmead
 */
public class PlayerInteract implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player player = (Player) e.getPlayer();

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (ElasticPlayer.getPlayer(player.getName()).getPermissions().getPower() > PermissionSet.JNR_STAFF.getPower()) {
                e.setCancelled(false);
            } else {
                e.setCancelled(true);
            }
            if (e.getItem() != null && e.getItem().hasItemMeta() && e.getItem().getItemMeta().hasDisplayName()) {
                if (ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Choose Team")) {
                    LaserQuestInventorys.getTeamSelector().open(player);
                }
                if (ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Gun Builder")) {
                    LaserQuestInventorys.getGunBuilderMain().open(player);
                }
                if (GameState.getGameState() == GameState.IN_GAME) {
                    if (LaserPlayer.getPlayer(player.getName()).isDeactivated()) {
                        Messaging.sendMessage(player, "&cYou're currently Deactivated!");
                        return;
                    }
                    if (LaserPlayer.getPlayer(player.getName()).getCoolDown() > 0) {
                        Messaging.sendMessage(player, "&5&lCOOLDOWN! &cYou must wait {0} seconds", LaserPlayer.getPlayer(player.getName()).getCoolDown());
                        return;
                    }
                    Gun gt = LaserPlayer.getPlayer(player.getName()).getGun();
                    if (gt == null) {
                        return;
                    }
                    if (e.getItem().getType() == gt.getGunCase().getMaterial()) {

                        Arrow arrow = player.shootArrow();
                        arrow.setVelocity(arrow.getVelocity().multiply(3));
                        Location start = player.getEyeLocation();
                        Vector increase = start.getDirection();
                        for (int counter = 0; counter < 100; counter++) {
                            Location point = start.add(increase);
                            if (gt.getLaser().getColor() == Color.WHITE) {
                                if (LaserPlayer.getPlayer(e.getPlayer().getName()).getTeam().getName() == "Red") {
                                    ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(Color.RED), point, 200D);
                                } else {
                                    ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(Color.GREEN), point, 200D);
                                }
                            } else {
                                ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(gt.getLaser().getColor()), point, 200D);
                            }
                            if (counter == 100) {
                                arrow.setVelocity(point.toVector());
                            }
                        }
                        LaserPlayer.getPlayer(player.getName()).setShots(LaserPlayer.getPlayer(player.getName()).getShots() + 1);
                        LaserPlayer.getPlayer(player.getName()).setCoolDown(5);
                    }

                }
            }
        }
    }
}

package ch.playat.rushmead.elasticmc.laserquest.events;

import ch.playat.rushmead.elasticmc.framework.api.ElasticCommand;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.customevents.FrameworkEnableEvent;
import ch.playat.rushmead.elasticmc.laserquest.commands.GameCommand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Rushmead
 */
public class FrameworkEnable implements Listener {

    @EventHandler
    public void onFrameworkEnable(FrameworkEnableEvent e) {
        ElasticCommand.registerCommand("laserquest", "game", new GameCommand());
        GameState.setGameState(GameState.LOBBY);
    }
}

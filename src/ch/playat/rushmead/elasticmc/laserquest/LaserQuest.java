package ch.playat.rushmead.elasticmc.laserquest;

import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.laserquest.events.FrameworkEnable;
import ch.playat.rushmead.elasticmc.laserquest.events.InventoryClick;
import ch.playat.rushmead.elasticmc.laserquest.events.OneMinute;
import ch.playat.rushmead.elasticmc.laserquest.events.OneSecond;
import ch.playat.rushmead.elasticmc.laserquest.events.ProjectileHit;
import ch.playat.rushmead.elasticmc.laserquest.events.ResourcePack;
import ch.playat.rushmead.elasticmc.laserquest.events.player.PickUpItem;
import ch.playat.rushmead.elasticmc.laserquest.events.player.PlayerDamage;
import ch.playat.rushmead.elasticmc.laserquest.events.player.PlayerInteract;
import ch.playat.rushmead.elasticmc.laserquest.events.player.PlayerJoin;
import ch.playat.rushmead.elasticmc.laserquest.events.player.PlayerLeave;
import ch.playat.rushmead.elasticmc.laserquest.events.player.PlayerMovement;
import ch.playat.rushmead.elasticmc.laserquest.game.GameManager;
import ch.playat.rushmead.elasticmc.laserquest.lobby.LobbyManager;
import ch.playat.rushmead.elasticmc.laserquest.maps.MapManager;
import ch.playat.rushmead.elasticmc.laserquest.teams.Team;
import ch.playat.rushmead.elasticmc.laserquest.teams.TeamManager;
import ch.playat.rushmead.elasticmc.laserquest.util.SimpleScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Rushmead
 */
public class LaserQuest extends JavaPlugin {

    private static LaserQuest instance;
    private TeamManager teamManager;
    private GameManager gameManager;
    private LobbyManager lobbyManager;
    private MapManager mapManager;

    public static LaserQuest getInstance() {
        return instance;
    }

    public void onEnable() {
        instance = this;
        PluginManager pm = Bukkit.getPluginManager();
        teamManager = new TeamManager();
        gameManager = new GameManager();
        lobbyManager = new LobbyManager();
        mapManager = new MapManager();

        pm.registerEvents(new FrameworkEnable(), this);
        pm.registerEvents(new OneMinute(), this);
        pm.registerEvents(new OneSecond(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new InventoryClick(), this);
        pm.registerEvents(new PlayerInteract(), this);
        pm.registerEvents(new ResourcePack(), this);
        pm.registerEvents(new PlayerDamage(), this);
        pm.registerEvents(new PickUpItem(), this);
        pm.registerEvents(new ProjectileHit(), this);
        pm.registerEvents(new PlayerMovement(), this);
        pm.registerEvents(new PlayerLeave(), this);
        new Team("Red", ChatColor.RED);
        new Team("Green", ChatColor.GREEN);
        mapManager.loadMaps();
        mapManager.pickMap();
        Servers.getMap().setMapName(mapManager.getCurrentMap().getMapName());
        SimpleScoreboard.getManager().start();
    }

    public TeamManager getTeamManager() {
        return teamManager;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public LobbyManager getLobbyManager() {
        return lobbyManager;
    }

    public MapManager getMapManager() {
        return mapManager;
    }

}

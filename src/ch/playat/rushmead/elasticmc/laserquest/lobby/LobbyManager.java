package ch.playat.rushmead.elasticmc.laserquest.lobby;

import ch.playat.rushmead.elasticmc.framework.api.GameState;

/**
 *
 * @author Rushmead
 */
public class LobbyManager {

    public boolean countdown = false;
    public int countDownTime = 60;

    public void startCountdown() {
        GameState.setGameState(GameState.LOBBY);
        countdown = true;
    }
}

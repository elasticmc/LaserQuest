package ch.playat.rushmead.elasticmc.laserquest.lobby;

import ch.playat.rushmead.elasticmc.framework.items.FrameworkItems;
import ch.playat.rushmead.elasticmc.framework.items.ItemUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Rushmead
 */
public class LobbyItems {

    public static ItemStack[] getInventory() {
        ItemStack[] items = new ItemStack[9];
        items[0] = ItemUtil.createItem(Material.BOOK, "&5Choose Team");
        items[1] = ItemUtil.createItem(Material.EMERALD, "&5Gun Builder");
        items[8] = FrameworkItems.backToHub();
        return items;
    }
}

package ch.playat.rushmead.elasticmc.laserquest.commands;

import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.laserquest.LaserQuest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class GameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player && ElasticPlayer.getPlayer(((Player) sender).getName()).getPermissions().getPower() < PermissionSet.SNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou do not have the permissions to run this command.");
            return false;
        }
        if (args.length == 0) {
            Messaging.sendMessage(sender, "&cIncorrect usage: /game start/stop/restart");
            return false;
        }
        String subcommand = args[0];

        switch (subcommand) {
            case "start":
                LaserQuest.getInstance().getLobbyManager().startCountdown();
                break;
            case "force":
                LaserQuest.getInstance().getGameManager().prep();
                break;
            case "stop":
                LaserQuest.getInstance().getGameManager().stop();
                break;
        }

        return true;
    }

}

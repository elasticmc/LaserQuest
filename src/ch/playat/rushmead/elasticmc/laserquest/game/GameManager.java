package ch.playat.rushmead.elasticmc.laserquest.game;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.framework.util.PlayerUtil;
import ch.playat.rushmead.elasticmc.laserquest.LaserQuest;
import ch.playat.rushmead.elasticmc.laserquest.game.guns.Gun;
import ch.playat.rushmead.elasticmc.laserquest.player.LaserPlayer;
import ch.playat.rushmead.elasticmc.laserquest.teams.Team;
import ch.playat.rushmead.elasticmc.laserquest.util.Scoreboard;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.ChatPaginator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rushmead
 */
public class GameManager {

    public boolean countdown = false;
    public int countDownTime = 10;
    // In Minutes
    public int gameTime = 5;

    public void start() {
        for (ElasticPlayer ep : ElasticPlayer.getPlayers().values()) {
            ep.setFrozen(false);

            PlayerUtil.sendTitleOrSubtitle(ep.getPlayer(), PacketPlayOutTitle.EnumTitleAction.TITLE, "GAME STARTED", 0, 20, 0, ChatColor.DARK_PURPLE);
        }
        Messaging.broadcastMessage("&5GAME STARTED!");

        GameState.setGameState(GameState.IN_GAME);
        countdown = false;
    }

    public void stop() {
        GameState.setGameState(GameState.ENDING);
        Team winner = null;
        String bullet = "\u2022";
        String bullets = ChatColor.DARK_GRAY + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet;
        if (LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getPoints() > LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getPoints()) {
            winner = LaserQuest.getInstance().getTeamManager().getTeamByName("Red");
        } else if (LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getPoints() == LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getPoints()) {

        } else {
            winner = LaserQuest.getInstance().getTeamManager().getTeamByName("Green");
        }
        Messaging.broadcastMessage("&5GAME ENDED!");
        for (ElasticPlayer ep : ElasticPlayer.getPlayers().values()) {
            ep.getPlayer().getInventory().clear();

            PlayerUtil.sendTitleOrSubtitle(ep.getPlayer(), PacketPlayOutTitle.EnumTitleAction.TITLE, "GAME ENDED!", 0, 20, 0, ChatColor.DARK_PURPLE);
        }
        Messaging.broadcastRawMessage(title(bullets));
        if (winner == null) {
            Messaging.broadcastRawMessage(title("&4Congrats to &lboth teams! It was a &lTie!"));
        } else {
            Messaging.broadcastRawMessage(title("&4Congrats to {0}{1} team on winning!"), winner.getColor(), winner.getName());
        }
        Messaging.broadcastRawMessage(title("{0}{1} Points: {2}"), LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getColor(), LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getName(), LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getPoints());
        Messaging.broadcastRawMessage(title("{0}{1} Points: {2}"), LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getColor(), LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getName(), LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getPoints());
        for (Player p : Bukkit.getOnlinePlayers()) {
            Messaging.sendRawMessage(p, title("{0}{1} Points: {2}"), ChatColor.GOLD, "Your", LaserPlayer.getPlayer(p.getName()).getPoints());
            Messaging.sendRawMessage(p, title("{0}ElasticBands Earnt: {2}"), ChatColor.GOLD, "Your", LaserPlayer.getPlayer(p.getName()).getPoints() / 5);
            int current = Players.getBands().getElasticBands(p.getName());
            Players.getBands().setBands(p.getName(), current + (LaserPlayer.getPlayer(p.getName()).getPoints() / 5));
            GameRank.tryLevelUp(LaserPlayer.getPlayer(p.getName()));
        }
        Messaging.broadcastRawMessage(title("&5Restarting Server in 10 Seconds...."));
        Messaging.broadcastRawMessage(title(bullets));
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restartserver 10");
    }

    public String title(String text) {

        String title = "";

        for (int x = 0; x <= ((ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH / 2) - text.length()); x++) {
            title += " ";
        }

        title += text;

        for (int x = 0; x <= ((ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH / 2) - text.length()); x++) {
            title += " ";
        }

        return title;

    }

    public void startCountdown() {
        GameState.setGameState(GameState.SETUP);
        countdown = true;
    }

    public org.bukkit.scoreboard.Team getSmallestTeam() {
        org.bukkit.scoreboard.Team smallest = (org.bukkit.scoreboard.Team) Scoreboard.getScoreboard().getTeams().toArray()[0];
        for (org.bukkit.scoreboard.Team t : Scoreboard.getScoreboard().getTeams()) {
            if (Scoreboard.getScoreboard().getTeam(t.getName()).getEntries().size() < Scoreboard.getScoreboard().getTeam(smallest.getName()).getEntries().size()) {
                smallest = t;
            }
        }
        return smallest;
    }

    public void prep() {

        for (Player p : Bukkit.getOnlinePlayers()) {
            LaserPlayer lp = LaserPlayer.getPlayer(p.getDisplayName());
            p.getInventory().clear();
            if (LaserQuest.getInstance().getTeamManager().hasTeam(p)) {
                lp.setTeam(LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p));
            } else {

                lp.setTeam(LaserQuest.getInstance().getTeamManager().getTeamByName(getSmallestTeam().getName()));
                LaserQuest.getInstance().getTeamManager().getTeamByName(getSmallestTeam().getName()).addPlayer(p);
            }

            LaserQuest.getInstance().getTeamManager().getScoreboardTeams().get(LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p)).addPlayer(p);
            if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName() == "Red") {
                LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation1(p);
            } else {
                LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation2(p);
            }
            p.setScoreboard(Scoreboard.getScoreboard());
            Gun gt = lp.getGun();
            if (gt == null) {

            } else {

                ItemStack gun = new ItemStack(gt.getGunCase().getMaterial());
                ItemMeta gunMeta = gun.getItemMeta();
                gunMeta.setDisplayName(ChatColor.GOLD + gt.getName());
                List<String> lore = new ArrayList<>();
                lore.add(LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getColor() + gt.getName());
                lore.add(Messaging.colorizeMessage("&4Barrel: &6&l" + gt.getBarrel().getName()));
                lore.add(Messaging.colorizeMessage("&4Cooler: &6&l" + gt.getCooler().getName()));
                lore.add(Messaging.colorizeMessage("&4Case: &6&l" + gt.getGunCase().getName()));
                lore.add(Messaging.colorizeMessage("&4Laser: &6&l" + gt.getLaser().getName()));
                gunMeta.setLore(lore);
                gun.setItemMeta(gunMeta);
                p.getInventory().addItem(gun);
                Scoreboard.game(p);
            }
        }
        for (ElasticPlayer ep : ElasticPlayer.getPlayers().values()) {
            ep.setFrozen(true);
        }

        startCountdown();
    }
}

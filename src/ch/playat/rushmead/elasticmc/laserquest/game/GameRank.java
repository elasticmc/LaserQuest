package ch.playat.rushmead.elasticmc.laserquest.game;

import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.framework.util.PlayerUtil;
import ch.playat.rushmead.elasticmc.laserquest.player.LaserPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.ChatColor;

/**
 * Created by Rushmead for ElasticMC
 */
public enum GameRank {

    PRIVATE(0, 0, "Private"),
    LANCE_CORPORAL(1, 50, "Lance Corporal"),
    SERGEANT(2, 100, "Sergeant"),
    STAFF_SERGEANT(3, 200, "Staff Sergeant"),
    SECOND_LIEUTENANT(4, 300, "Second Lieutenant"),
    LIEUTENANT(5, 400, "Lieutenant"),
    CAPTAIN(6, 500, "Captain"),
    MAJOR(7, 600, "Major"),
    LIEUTENANT_COLONEL(8, 700, "Lieutenant Colonel"),
    COLONEL(9, 800, "Colonel");
    private int id;
    private int kills;
    private String displayName;

    GameRank(int id, int kills, String displayName) {
        this.id = id;
        this.kills = kills;
        this.displayName = displayName;
    }

    public int getId() {
        return id;
    }

    public int getKills() {
        return kills;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static GameRank getRankFromID(int id) {
        for (GameRank gr : GameRank.values()) {
            if (gr.getId() == id) {
                return gr;
            }
        }
        return GameRank.PRIVATE;
    }

    public static void tryLevelUp(LaserPlayer lp) {
        boolean levelUp = false;
        for (GameRank gr : GameRank.values()) {
            if (lp.getTotalPoints() >= gr.getKills() && gr.getId() > lp.getGameRank().getId() && !levelUp) {
                lp.setGameRank(gr);
                levelUp = true;
            }
        }
        if (levelUp) {
            PlayerUtil.sendTitleOrSubtitle(lp.getPlayer(), PacketPlayOutTitle.EnumTitleAction.TITLE, "Congrats, You Leveled up!", 0, 60, 20, ChatColor.AQUA);
            Messaging.sendMessage(lp.getPlayer(), "&bYou Leveled up! Your now &6&l{0}", lp.getGameRank().getDisplayName());
            Messaging.broadcastMessage("&6&l{0}&r &bhas leveled up to &6&l{1}", lp.getRealName(), lp.getGameRank().getDisplayName());
        }

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.playat.rushmead.elasticmc.laserquest.game;

/**
 *
 * @author Stuart
 */
public enum PartType {
    LASER,
    COOLER,
    CASE,
    BARREL
}
